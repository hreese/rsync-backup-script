$rsyncconfig = @{
    # Copy files from these directories. End with backslash to copy the directory's
    # content, skip trailing backslash to copy the directory itself:
    # "foo\bar" -> destination/bar
    # "foobar\" -> destination/{files from foobar}
    sourceDirectories    = @(
        "~\Documents\test with spaces\"
        "~\Downloads\_fonts"
    );
    # what to do when a source path does not exist
    sourceMissingAction  = [FailureMode]::Exit;
    # destination directory relative to destination drive's root
    destinationDirectory = "Wolfgang\";
    # file that has to exist on destination (relative to drive's root)
    mountpointIndicator  = ".is_mounted";
    # path to rsync binary (MSYS2)
    # see https://github.com/rivy/scoop-bucket/blob/master/rsync.json for download urls
    rsyncBinary          = Convert-Path "C:\cygwin64\home\$env:USERNAME\rsync\usr\bin\rsync.exe";
    rsyncParams          = "-P", "-r", "-u", "-l", "-k", "-v";
}

# Hide indicator file with
# attrib.exe +r +s +h "O:\.is_mounted"
