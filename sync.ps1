# Fail on errors
$ErrorActionPreference = "Stop"

# types
Add-Type -TypeDefinition @"
   public enum FailureMode
   {
      Ignore,
      SkipWarn,
      Exit,
   }
"@

function quit_and_wait($status) {
    Write-Host -NoNewLine 'Press any key to continue...';
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
    exit $status;
}

# Configuration
. .\config.ps1

# convert windows path (starting with drive letter) to MSYS2 compatible path
function PathToMSYS2($path) {
    # canonicalize path
    $path = Convert-Path $path
    # split path into single components
    $parts = $path.Split([system.io.path]::DirectorySeparatorChar)
    # lowercase drive letter, remove colon, prepend slash (X: -> /x)
    $parts[0] = $parts[0].ToLower() -replace ':', '' -replace '^', '/'
    # join with forward slash to create an MSYS2/UNIX path
    $parts -join '/'
}

# check sources and convert for rsync usage
$sourcesMSYS2 = New-Object System.Collections.Generic.List[System.Object]
$rsyncconfig.sourceDirectories | ForEach-Object {
    if (-Not (Test-Path -LiteralPath $_ -PathType Container)) {
        switch ($rsyncconfig.sourceMissingAction) {
            [FailureMode]::SkipWarn {
                Write-Host "Source directory $_ does not exist, skipping."
            }
            [FailureMode]::Exit {
                Write-Host "Source directory $_ does not exist."
                quit_and_wait(1)
            }
        }
    }
    else {
        $convertedPath = PathToMSYS2($_)
        $sourcesMSYS2.Add($convertedPath)
    }
}

# check if rsync.exe exists
if ( -Not ( Test-Path -LiteralPath $rsyncconfig.rsyncBinary -PathType Leaf )) {
    Write-Host "Unable to find rsync binary at $rsyncconfig.rsyncBinary."
    quit_and_wait(2)
}

# check all mounts for a matching filesystem and run rsync on matches
Get-PSDrive -PSProvider filesystem | ForEach-Object {
    $drive = $_.root
    # check existence of indicator file
    if (Join-Path -Path $drive -ChildPath $rsyncconfig.mountpointIndicator | Test-Path ) {
        # generate and clean destination path
        $destination = Join-Path -Path $drive -ChildPath $rsyncconfig.destinationDirectory
        # check if destination path exists, create if needed
        if ( -Not (Test-Path -PathType Container $destination )) {
            New-Item -ItemType Directory $destination
        }
        Write-Host "# Found destination ${destination}"
        $destination = PathToMSYS2($destination) + '/'
        # copy files using rsync
        & $rsyncconfig.rsyncBinary $rsyncconfig.rsyncParams $sourcesMSYS2 $destination
    }
}

quit_and_wait(0)

